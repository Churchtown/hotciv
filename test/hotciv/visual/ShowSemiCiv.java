package hotciv.visual;

import hotciv.factories.SemiCivFactory;
import hotciv.framework.Game;
import hotciv.standard.GameImpl;
import hotciv.standard.Transcriber;
import minidraw.framework.DrawingEditor;
import minidraw.standard.MiniDrawApplication;
import minidraw.standard.SelectionTool;
import tools.*;

public class ShowSemiCiv {
    public static void main(String[] args) {
        Transcriber transcriber = new Transcriber(new GameImpl(new SemiCivFactory()));
        Game game = transcriber.getGame();

        DrawingEditor editor =
                new MiniDrawApplication( "SemiCiv - Play as you wish",
                        new HotCivFactory4(game) );
        editor.open();
        editor.showStatus("Enjoy the game!");


        editor.setTool( new SelectionTool(editor) );
        CompositionTool compTool = new CompositionTool();
        compTool.addTool(new ActionTool(game));
        compTool.addTool(new EndOfTurnTool(editor,game));
        compTool.addTool(new UnitMoveTool(editor,game));
        compTool.addTool(new TileFocusTool(game));

        editor.setTool(compTool);
    }
}
