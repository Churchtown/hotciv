package hotciv.standard;
import hotciv.strategies.agingStrategies.IntervalAgingStrategy;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by soren_kirkeby on 20/09/2017.
 */
public class TestIntervalAgingStrategy {

    private IntervalAgingStrategy intervalAgingStrategy;

    @Before
    public void setUp() throws Exception {
        intervalAgingStrategy = new IntervalAgingStrategy();

    }

    @Test
    public void ageShouldBeMinus100AfterMinus200(){
        assertThat(intervalAgingStrategy.age(-200),is(-100));
    }

    @Test
    public void ageShouldBeMinus1AfterMinus100(){
        assertThat(intervalAgingStrategy.age(-100),is(-1));
    }

    @Test
    public void ageShouldBe1AfterMinus1(){
        assertThat(intervalAgingStrategy.age(-1),is(1));
    }

    @Test
    public void ageShouldBe50After1(){
        assertThat(intervalAgingStrategy.age(1),is(50));
    }

    @Test
    public void ageShouldBe100After50(){
        assertThat(intervalAgingStrategy.age(50),is(100));
    }

    @Test
    public void ageShouldBe1775After1750(){
        assertThat(intervalAgingStrategy.age(1750),is(1775));
    }

    @Test
    public void ageShouldBe1905After1900(){
        assertThat(intervalAgingStrategy.age(1900),is(1905));
    }

    @Test
    public void agShouldBe1971After1970() {
        assertThat(intervalAgingStrategy.age(1970), is(1971));

    }
}
