package hotciv.standard;

import hotciv.factories.SemiCivFactory;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by soren_kirkeby on 13/10/2017.
 * These are smoke tests to ensure that the different strategies don't break the game
 */
public class TestSemiCivIntegration {
    Game game;
    @Before
    public void setUp() throws Exception {
       game = new GameImpl(new SemiCivFactory());

    }

    @Test
    public void shouldMoveRedArcher() {
        assertTrue(game.moveUnit(new Position(3,8), new Position(4,8)));
    }

    @Test
    public void shouldChangeProductionInRedCityToArcher() {
        game.changeProductionInCityAt(new Position(8,12), GameConstants.ARCHER);
        assertThat(game.getCityAt(new Position(8,12)).getProduction(), is(GameConstants.ARCHER));

    }
}
