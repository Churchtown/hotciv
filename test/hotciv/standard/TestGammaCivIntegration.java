package hotciv.standard;
import hotciv.factories.GammaCivFactory;
import hotciv.framework.*;
import hotciv.strategies.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */

public class TestGammaCivIntegration {

    private GameImpl game;

    @Before
    public void setUp(){

        game = new GameImpl(new GammaCivFactory());

    }

    @Test
    public void redSettlerShouldProduceRedCityWithUnitAction(){
        Position current = new Position(4,3);
        assertThat(game.getCityAt(current),is(nullValue()));
        game.performUnitActionAt(current);
        assertThat(game.getUnitAt(current),is(nullValue()));
        assertThat(game.getCityAt(current).getOwner(), is(Player.RED));
    }

    @Test
    public void archerShouldHave6DefensiveStrengthWhenFortifyingWithUnitAction(){
        Position current = new Position(2,0);
        game.performUnitActionAt(current);
        assertThat(game.getUnitAt(current).getDefensiveStrength(),is(6));
    }

    @Test
    public void archerShouldNotMoveWhenFortified(){
        Position current = new Position(2,0);
        game.performUnitActionAt(current);
        assertThat(game.moveUnit(current,new Position(2,1)),is(false));
    }

    @Test
    public void archerShouldHave3DefenceIfFortifiedTwiceAndBeAbleToMove(){
        Position current = new Position(2,0);
        game.performUnitActionAt(current);
        game.performUnitActionAt(current);
        assertThat(game.getUnitAt(current).getDefensiveStrength(),is(3));
        assertThat(game.moveUnit(current,new Position(2,1)),is(true));
    }
}
