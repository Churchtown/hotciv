package hotciv.standard;
import hotciv.factories.AlphaCivFactory;
import hotciv.framework.*;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


/** Skeleton class for AlphaCiv test cases

 Updated Oct 2015 for using Hamcrest matchers

 This source code is from the book
 "Flexible, Reliable Software:
 Using Patterns and Agile Development"
 published 2010 by CRC Press.
 Author:
 Henrik B Christensen
 Department of Computer Science
 Aarhus University

 Please visit http://www.baerbak.com/ for further information.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 */
public class TestAlphaCiv {
    private Game game;

    /** Fixture for alphaciv testing. */
    @Before
    public void setUp() {
        game = new GameImpl(new AlphaCivFactory());
    }

    // Player tests
    @Test
    public void shouldBeRedAsStartingPlayer() {
        assertThat(game, is(notNullValue()));
        assertThat(game.getPlayerInTurn(), is(Player.RED));
    }

    @Test
    public void shouldBeBlueAfterRedSecondTurn(){
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
    }

    @Test
    public void shouldBeRedAfterBlueThirdTurn() {
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.RED));
    }

    // The game only switches between red and blue, so they must be the only two players
    @Test
    public void shouldOnlyBeTwoPlayersRedAndBlue() {
        assertThat(game.getPlayerInTurn(), is(Player.RED));
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.RED));
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
    }

    // Map tests
    @Test
    public void tile0_0shouldBeUnoccupied() {

        assertThat(game.getUnitAt(new Position(0,0)), is(nullValue()));
    }

    @Test
    public void tile2_0shouldBeOccupied() {

        assertThat(game.getUnitAt(new Position(2,0)), is(notNullValue()));
    }

    @Test
    public void tile0_7ShouldBePlains() {
        assertThat(game.getTileAt(new Position(0,7)).getTypeString(), is(GameConstants.PLAINS));
    }

    @Test
    public void tile0_8shouldBePlains() {
        assertThat(game.getTileAt(new Position(0,8)).getTypeString(), is(GameConstants.PLAINS));
    }

    @Test
    public void tile1_0shouldBeOcean() {

        assertThat(game.getTileAt(new Position(1,0)).getTypeString(), is(GameConstants.OCEANS));
    }

    @Test
    public void tile0_1shouldBeHills() {

        assertThat(game.getTileAt(new Position(0,1)).getTypeString(), is(GameConstants.HILLS));
    }

    @Test
    public void tile2_2shouldBeMountain() {

        assertThat(game.getTileAt(new Position(2,2)).getTypeString(), is(GameConstants.MOUNTAINS));
    }

    // Unit tests
    @Test
    public void shouldBeRedArcherAt2_0() {

        assertThat(game.getUnitAt(new Position(2,0)).getTypeString(), is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(2,0)).getOwner(), is(Player.RED));
    }

    @Test
    public void shouldBeBlueLegionAt3_2() {

        assertThat(game.getUnitAt(new Position(3,2)).getTypeString(), is(GameConstants.LEGION));
        assertThat(game.getUnitAt(new Position(3,2)).getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldBeRedSettlerAt4_3() {

        assertThat(game.getUnitAt(new Position(4,3)).getTypeString(), is(GameConstants.SETTLER));
        assertThat(game.getUnitAt(new Position(4,3)).getOwner(), is(Player.RED));
    }

    @Test
    public void shouldMoveUnitAt2_0To2_1() {
        Position startPos = new Position(2,0);
        Position endPos = new Position(2,1);
        assertThat(game.getPlayerInTurn(), is(Player.RED));

        // check for unit at start-position, end-position is empty
        assertThat(game.getUnitAt(startPos),is(notNullValue()));
        assertThat(game.getUnitAt(endPos), is(nullValue()));

        //move unit
        assertThat(game.moveUnit(startPos,endPos),is(true));

        // check for unit at end-position, start-position is empty
        assertThat(game.getUnitAt(endPos),is(notNullValue()));
        assertThat(game.getUnitAt(startPos), is(nullValue()));

    }

    @Test
    public void redArcherShouldNotMoveTwoTilesInOneMove(){
        Position startPos = new Position(2,0);
        Position endPos = new Position(2,2);

        assertThat(game.moveUnit(startPos,endPos),is(false));

        assertThat(game.getUnitAt(startPos),is(notNullValue()));
        assertThat(game.getUnitAt(endPos), is(nullValue()));
    }

    @Test
    public void redArcherShouldNotMoveTwoTimesInOneTurn() {
        assertThat(game.moveUnit(new Position(2,0), new Position(2,1)), is(true));
        assertThat(game.moveUnit(new Position(2,1), new Position(2,0)), is(false));
    }

    @Test
    public void redArcherShouldReceiveMovementPointsAfterRound() {
        assertThat(game.moveUnit(new Position(2,0), new Position(2,1)), is(true));
        assertThat(game.moveUnit(new Position(2,1), new Position(2,0)), is(false));
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.moveUnit(new Position(2,1), new Position(2,0)), is(true));


    }

    @Test
    public void shouldOnlyBeOneUnitAllowedPerTile(){
        assertThat(game.moveUnit(new Position(2,0),new Position(3,1)), is(true));
        assertThat(game.moveUnit(new Position(3,2),new Position(3,1)),is(false));

    }

    @Test
    public void shouldNotMoveUnitAt3_2ToMountainAt2_2(){
        //end turn so it is blue's turn
        game.endOfTurn();

        assertThat(game.moveUnit(new Position(3,2),new Position(2,2)),is(false));
    }

    @Test
    public void shouldNotMoveUnitAt2_0ToOceanAt1_0(){
        assertThat(game.moveUnit(new Position(2,0),new Position(1,0)),is(false));
    }

    @Test
    public void redShouldNotMoveBlueLegionAt3_2WhileRedsTurn() {
        assertThat(game.getPlayerInTurn(), is(Player.RED));
        assertThat(game.getUnitAt(new Position(3,2)).getOwner(), is(Player.BLUE));
        assertThat(game.moveUnit(new Position(3,2), new Position(3,1)), is(false));
    }

    @Test
    public void blueShouldNotMoveRedArcherAt2_0WhileBluesTurn() {
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
        assertThat(game.moveUnit(new Position(2,0), new Position(2,1)), is(false));
    }

    // attacking

    @Test
    public void blueLegionShouldWinOverRedArcher() {
        game.moveUnit(new Position(2,0), new Position(2,1));
        assertThat(game.getUnitAt(new Position(2,1)).getOwner(), is(Player.RED));
        game.endOfTurn();
        assertThat(game.moveUnit(new Position(3,2), new Position(2,1)), is(true));
        assertThat(game.getUnitAt(new Position(2,1)).getOwner(), is(Player.BLUE));

    }

    @Test
    public void redArcherShouldWinOverBlueLegion() {
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
        game.moveUnit(new Position(3,2), new Position(2,1));
        game.endOfTurn();
        assertThat(game.moveUnit(new Position(2,0), new Position(2,1)), is(true));
        assertThat(game.getUnitAt(new Position(2,1)).getTypeString(), is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(2,1)).getOwner(), is(Player.RED));
    }

    //City tests

    @Test
    public void shouldBeRedCityAt1_1() {

        assertThat(game.getCityAt(new Position(1,1)), is(notNullValue()));
        assertThat(game.getCityAt(new Position(1,1)).getOwner(), is(Player.RED));
    }

    @Test
    public void shouldBeBlueCityAt4_1() {
        assertThat(game.getCityAt(new Position(4,1)), is(notNullValue()));
        assertThat(game.getCityAt(new Position(4,1)).getOwner(), is(Player.BLUE));
    }

    @Test
    public void city4_1ShouldBeSize1() {
        assertThat(game.getCityAt(new Position(4,1)).getSize(), is(1));
    }

    @Test
    public void city1_1shouldBeSize1() {
        assertThat(game.getCityAt(new Position(1,1)).getSize(), is(1));
    }

    @Test
    public void city1_1ShouldStillBeSize1AfterTwoRounds() {
        assertThat(game.getCityAt(new Position(1,1)).getSize(), is(1));
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getCityAt(new Position(1,1)).getSize(), is(1));
    }

    @Test
    public void canProduceArcherInCity1_1(){
        game.changeProductionInCityAt(new Position(1,1),GameConstants.ARCHER);
        assertThat(game.getCityAt(new Position(1,1)).getProduction(), is(GameConstants.ARCHER));
    }

    @Test
    public void canProduceLegionInCity1_1(){
        game.changeProductionInCityAt(new Position(1,1),GameConstants.LEGION);
        assertThat(game.getCityAt(new Position(1,1)).getProduction(), is(GameConstants.LEGION));
    }

    @Test
    public void canProduceSettlerInCity1_1(){
        game.changeProductionInCityAt(new Position(1,1),GameConstants.SETTLER);
        assertThat(game.getCityAt(new Position(1,1)).getProduction(), is(GameConstants.SETTLER));
    }

    @Test
    public void redArcherShouldControlBlueCityAfterEntering() {
        assertThat(game.getCityAt(new Position(4,1)).getOwner(), is(Player.BLUE));
        game.moveUnit(new Position(2,0), new Position(3,0));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(3,0), new Position(4,1));
        assertThat(game.getCityAt(new Position(4,1)).getOwner(), is(Player.RED));
    }

    @Test
    public void blueLegionShouldControlRedCityAfterEntering() {
        assertThat(game.getCityAt(new Position(1,1)).getOwner(), is(Player.RED));
        game.endOfTurn();
        game.moveUnit(new Position(3,2), new Position(2,1));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(2,1), new Position(1,1));
        assertThat(game.getCityAt(new Position(1,1)).getOwner(), is(Player.BLUE));
    }

    // Archer costs 10 production, so it will be made after two rounds.
    // city produces 6 production per round
    @Test
    public void shouldProduceOneArcherAfterTwoRoundsAndPlacedInCity1_1() {
        assertThat(game.getUnitAt(new Position(1,1)), is(nullValue()));
        game.changeProductionInCityAt(new Position(1,1), GameConstants.ARCHER);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getUnitAt(new Position(1,1)).getTypeString(), is(GameConstants.ARCHER));
    }

    // cost of archer is deducted from the city's current production.
    // north of the city has to be empty as that is the field the next archer would be produced.
    @Test
    public void shouldProduceOneArcherAfterTwoRoundsAndNotProduceAnArcherAfterAnotherRoundNorthOfCity1_1() {
        assertThat(game.getUnitAt(new Position(1,1)), is(nullValue()));
        game.changeProductionInCityAt(new Position(1,1), GameConstants.ARCHER);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getUnitAt(new Position(1,1)).getTypeString(), is(GameConstants.ARCHER));
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getUnitAt(new Position(0,1)), is(nullValue()));
    }

    @Test
    public void shouldProduceFourthArcherEastOfCity1_1After7Rounds() {
        assertThat(game.getUnitAt(new Position(1,1)), is(nullValue()));
        game.changeProductionInCityAt(new Position(1,1), GameConstants.ARCHER);

        //end turn 14 times = 7 rounds
        for(int i=0; i<14; i++){
            game.endOfTurn();
        }

        assertThat(game.getUnitAt(new Position(1,2)).getTypeString(), is(GameConstants.ARCHER));
    }

    //tests that a unit can't be placed on 1-0 as that is an ocean tile
    //tests that when the first 8 fields are occupied or are mountain/ocean, then the 9th tile is north-west of the city
    @Test
    public void shouldProduceEightsArcherNorthWestOfCity1_1After14Rounds() {
        assertThat(game.getUnitAt(new Position(1,1)), is(nullValue()));
        game.changeProductionInCityAt(new Position(1,1), GameConstants.ARCHER);

        //end turn 28 times = 14 rounds
        for(int i=0; i<28; i++){
            game.endOfTurn();
        }

        assertThat(game.getUnitAt(new Position(0,0)).getTypeString(), is(GameConstants.ARCHER));
    }

    @Test
    public void shouldProduceLegionInBlueCityAfterThreeRounds() {
        assertThat(game.getUnitAt(new Position(4,1)), is(nullValue()));
        game.changeProductionInCityAt(new Position(4,1), GameConstants.LEGION);
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getUnitAt((new Position(4,1))).getTypeString(), is(GameConstants.LEGION));
    }

    @Test
    public void shouldProduceSettlerInBlueCityAfterFiveRounds() {
        assertThat(game.getUnitAt(new Position(4,1)), is(nullValue()));
        game.changeProductionInCityAt(new Position(4,1), GameConstants.SETTLER);

        //end turn 10 times = 5 rounds
        for(int i=0; i<10; i++){
            game.endOfTurn();
        }

        assertThat(game.getUnitAt((new Position(4,1))).getTypeString(), is(GameConstants.SETTLER));
    }

    @Test
    public void shouldPutArcherNorthOfRedCityWhileOccupied() {
        game.changeProductionInCityAt(new Position(1,1), GameConstants.ARCHER);
        assertThat(game.getUnitAt(new Position(1,1)), is(nullValue()));
        assertThat(game.getUnitAt(new Position(0,1)), is(nullValue()));
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getUnitAt(new Position(1,1)), is(notNullValue()));
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getUnitAt(new Position(0,1)), is(notNullValue()));
    }

    //Aging tests
    @Test
    public void shouldStartAge4000BC() {
        assertThat(game.getAge(), is(-4000));
    }


    @Test
    public void shouldAge100EveryRound() {
        // a round occurs after two rounds
        for(int i=1;i<10;i++){
            game.endOfTurn();
            game.endOfTurn();
            assertThat(game.getAge(), is(-4000+i*100));
        }
    }

    @Test
    public void shouldBeNoWinnerInBeginning(){
        assertThat(game.getWinner(), is(nullValue()));
    }

    @Test
    public void redShouldWinAt3000BC(){
        for(int i=0; i<100; i++){
            game.endOfTurn();
            if(game.getAge()==-3000){
                assertThat(game.getWinner(), is(Player.RED));
            }
        }
    }
}
