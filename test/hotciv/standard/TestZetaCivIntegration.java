package hotciv.standard;

import hotciv.factories.ZetaCivWithTestMapFactory;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public class TestZetaCivIntegration {
    Game game;
    @Before
    public void setUp() throws Exception {
        game = new GameImpl(new ZetaCivWithTestMapFactory());
    }

    @Test
    public void redShouldNotWinAfterThreeSuccessfulAttacksBeforeRound20() {
        game.moveUnit(new Position(2,5), new Position(3,5));
        game.moveUnit(new Position(2,6), new Position(3,6));
        game.moveUnit(new Position(2,7), new Position(3,7));
        assertThat(game.getWinner(), is(nullValue()));
    }

    @Test
    public void redShouldWinAfterHoldingAllCitiesBeforeRound20() {
        game.moveUnit(new Position(4,2), new Position(4,1));
        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void redShouldNotWinAfterRound20HavingThreeSuccessfulAttacksBeforeRound20() {

        game.moveUnit(new Position(2,5), new Position(3,5));
        game.moveUnit(new Position(2,6), new Position(3,6));
        game.moveUnit(new Position(2,7), new Position(3,7));
        for(int i=0; i<42; i++) {
            game.endOfTurn();
        }
        assertThat(game.getWinner(), is(nullValue()));
    }

    @Test
    public void redShouldWinHavingThreeSuccessfulAttacksAfterRound20() {
        for(int i=0; i<42; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(2,5), new Position(3,5));
        game.moveUnit(new Position(2,6), new Position(3,6));
        game.moveUnit(new Position(2,7), new Position(3,7));
        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void redShouldNotWinHavingAllCitesAfterRound20() {
        for(int i=0; i<42; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(4,2), new Position(4,1));
        assertThat(game.getWinner(), is(nullValue()));

    }
}
