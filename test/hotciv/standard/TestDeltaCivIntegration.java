package hotciv.standard;
import hotciv.factories.DeltaCivFactory;
import hotciv.framework.*;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class TestDeltaCivIntegration {

    private Game game;
    
    @Before
    public void setUp() throws Exception {

        game = new GameImpl(new DeltaCivFactory());

    }

    //Alpha has Plains at 0-0. Test for that DeltaCiv has Ocean.
    @Test
    public void shouldBeOceanAt0_0NotPlains() {
        assertThat(game.getTileAt(new Position(0,0)).getTypeString(), is(GameConstants.OCEANS));

    }

    //Check for correct city placement in DeltaCiv
    @Test
    public void shouldBeRedCityAt8_12() {
        assertThat(game.getCityAt(new Position(8,12)).getOwner(), is(Player.RED));
    }

    //Check for correct unit placement in DeltaCiv
    @Test
    public void shouldBeRedArcherAt3_8() {
        assertThat(game.getUnitAt(new Position(3,8)).getTypeString(), is(GameConstants.ARCHER));
        assertThat(game.getUnitAt(new Position(3,8)).getOwner(), is(Player.RED));
    }
}
