package hotciv.standard;
import hotciv.factories.AlphaCivFactory;
import hotciv.framework.*;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class TestTranscription {

    private Game game;
    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private String newLine;

    @Before
    public void setUp() {
        game = new Transcriber(new GameImpl(new AlphaCivFactory()));
        System.setOut(new PrintStream(out));
        newLine = System.lineSeparator();
    }

    @After
    public void cleanUpStreams(){
        System.setOut(null);
    }

    @Test
    public void testMove() {
        game.moveUnit(new Position(2,0),new Position(2,1));
        assertEquals("RED moves archer from [2,0] to [2,1]." + newLine,out.toString());
    }

    @Test
    public void testEndTurn() {
        game.endOfTurn();
        assertEquals("RED ends turn." + newLine,out.toString());
    }

    @Test
    public void testChangeProduction() {
        game.changeProductionInCityAt(new Position(1,1),GameConstants.SETTLER);
        assertEquals("RED changes production in city at [1,1] to settler." + newLine,out.toString());
    }

    @Test
    public void testUnitAction() {
        game.performUnitActionAt(new Position(2,0));
        assertEquals("RED archer performs its unit action." + newLine,out.toString());
    }
}
