package hotciv.standard;
import hotciv.framework.*;
import hotciv.strategies.mapStrategies.DynamicMapStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class TestDynamicMapStrategy {
    private MapStrategy dynamicMapStrategy;
    private MapImpl map;


    @Before
    public void setUp() throws Exception {
        dynamicMapStrategy = new DynamicMapStrategy();
        map = dynamicMapStrategy.makeMap();

    }

    @Test
    public void shouldBeOceanAt0_0() {
        assertThat(map.getTileType(new Position(0,0)), is(GameConstants.OCEANS));

    }

    @Test
    public void shouldBePlainsAt0_3() {
        assertThat(map.getTileType(new Position(0,3)), is(GameConstants.PLAINS));

    }

    @Test
    public void shouldBeMountainAt0_5() {
        assertThat(map.getTileType(new Position(0,5)), is(GameConstants.MOUNTAINS));
    }

    @Test
    public void shouldBeHillAt1_3() {
        assertThat(map.getTileType(new Position(1,3)), is(GameConstants.HILLS));
    }

    @Test
    public void shouldBeForestAt5_2() {
        assertThat(map.getTileType(new Position(5,2)), is(GameConstants.FOREST));
    }

    @Test
    public void shouldBeRedCityAt8_12() {
        assertThat(map.getCityAt(new Position(8,12)).getOwner(), is(Player.RED));
    }

    @Test
    public void shouldBeBlueCityAt4_5() {
        assertThat(map.getCityAt(new Position(4,5)).getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldBeRedArcherAt3_8() {
        assertThat(map.getUnitType(new Position(3,8)), is(GameConstants.ARCHER));
    }

    @Test
    public void shouldBeBlueLegionAt4_4() {
        assertThat(map.getUnitMap().get(new Position(4,4)).getTypeString(), is(GameConstants.LEGION));

    }
}
