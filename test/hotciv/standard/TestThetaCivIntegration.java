package hotciv.standard;

import hotciv.factories.ThetaCivWithTestMapFactory;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;

import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by soren_kirkeby on 13/10/2017.
 */
public class TestThetaCivIntegration {
    private Game game;

    @Before
    public void setUp() throws Exception {
        game = new GameImpl(new ThetaCivWithTestMapFactory());

    }

    @Test
    public void shouldMoveGalleyTwoTiles() {
        assertTrue(game.moveUnit(new Position(4,0), new Position(4,1)));
        assertTrue(game.moveUnit(new Position(4,1), new Position(4,2)));

    }

    @Test
    public void shouldNotMoveGalleyThreeTiles() {
        assertTrue(game.moveUnit(new Position(4,0), new Position(4,1)));
        assertTrue(game.moveUnit(new Position(4,1), new Position(4,2)));
        assertFalse(game.moveUnit(new Position(4,2), new Position(4,3)));
    }

    @Test
    public void shouldNotMoveGalleyToPlains() {
        assertFalse(game.moveUnit(new Position(2,0), new Position(1,0)));

    }

    @Test
    public void redGalleyShouldWinOverBlueGalley() {
        assertTrue(game.moveUnit(new Position(2,0), new Position(3,0)));

    }


    @Test
    public void redCityAt2_2ShouldProduceGalley(){
        game.changeProductionInCityAt(new Position(2,2), GameConstants.GALLEY);
        for(int i=0; i<10; i++){
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(1,2)),is(notNullValue()));
    }

    @Test
    public void shouldNotProduceGalleyInCityNotOnCoast() {
        game.changeProductionInCityAt(new Position(11,11), GameConstants.GALLEY);
        for(int i=0; i<10; i++){
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(11,11)), is(nullValue()));
    }

    // Alternating spawning should work out that it's supposed to spawn archer on land
    @Test
    public void shouldSpawnArcherInCityAt11_11() {
        game.changeProductionInCityAt(new Position(11,11), GameConstants.ARCHER);
        for(int i=1; i < 6; i++) {
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(11,11)).getTypeString(), is(GameConstants.ARCHER));

    }
}
