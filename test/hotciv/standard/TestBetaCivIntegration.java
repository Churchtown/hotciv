package hotciv.standard;
import hotciv.factories.BetaCivFactory;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class TestBetaCivIntegration {

    private Game game;


    @Before
    public void setUp() {

        game = new GameImpl(new BetaCivFactory());
    }


    //Age after 40 rounds is 0 in Alpha but -1 in Beta
    //80 end turn = 40 rounds :)
    @Test
    public void ageShouldBeMinus1Not0after40rounds(){

        for(int i=0; i<80; i++){
            game.endOfTurn();
        }
        assertThat(game.getAge(),is(-1));
    }

    //There is a blue AND a red city at game start, thus the game should return no winner.
    //Should have been a unit-test, but can not be tested without a game implementation
    @Test
    public void shouldBeNoWinnerWhenBothRedAndBlueCityExists(){
        assertThat(game.getWinner(),is(nullValue()));
    }

    //Winner is red when he has both cities (not like Alpha where there is no winner after 1 round)
    @Test
    public void winnerShouldBeRedWhenRedOwnsBothCities() {
        game.moveUnit(new Position(2,0), new Position(3,0));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(3,0), new Position(4,1));
        assertThat(game.getWinner(), is(Player.RED));
    }


    @Test
    public void winnerShouldBeBlueWhenHeOwnsBothCities(){
        game.endOfTurn();
        game.moveUnit(new Position(3,2), new Position(2,1));
        game.endOfTurn();
        game.endOfTurn();
        game.moveUnit(new Position(2,1), new Position(1,1));
        assertThat(game.getWinner(), is(Player.BLUE));
    }
}
