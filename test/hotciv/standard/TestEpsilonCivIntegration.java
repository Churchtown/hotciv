package hotciv.standard;

import hotciv.factories.EpsilonFixedDieRollFactory;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.strategies.attackingStrategies.FixedDieRollStrategy;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public class TestEpsilonCivIntegration {


    @Test
    public void redShouldWinAfterWinningThreeAttacks() {
        FixedDieRollStrategy fixedDieRollStrategy = new FixedDieRollStrategy(6,1);
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        game.moveUnit(new Position(2,5), new Position(3,5));
        game.moveUnit(new Position(2,6), new Position(3,6));
        game.moveUnit(new Position(2,7), new Position(3,7));
        assertThat(game.getWinner(), is(Player.RED));

    }

    @Test
    public void blueShouldWinAfterWinningThreeAttacks() {
        FixedDieRollStrategy fixedDieRollStrategy = new FixedDieRollStrategy(6,1);
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        game.endOfTurn();
        game.moveUnit(new Position(3,5), new Position(2,5));
        game.moveUnit(new Position(3,6), new Position(2,6));
        game.moveUnit(new Position(3,7), new Position(2,7));
        assertThat(game.getWinner(), is(Player.BLUE));

    }
}
