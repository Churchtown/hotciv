package hotciv.standard;

import hotciv.factories.EpsilonFixedDieRollFactory;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.strategies.agingStrategies.AgingStrategy;
import hotciv.strategies.agingStrategies.LinearAgingStrategy;
import hotciv.strategies.attackingStrategies.AdvancedAttackingStrategy;
import hotciv.strategies.attackingStrategies.AttackingStrategy;
import hotciv.strategies.attackingStrategies.FixedDieRollStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.mapStrategies.SimpleMapStrategy;
import hotciv.strategies.unitActionStrategies.NoUnitActionStrategy;
import hotciv.strategies.unitActionStrategies.UnitActionStrategy;
import hotciv.strategies.winnerStrategies.Red3000WinnerStrategy;
import hotciv.strategies.winnerStrategies.WinnerStrategy;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by soren_kirkeby on 02/10/2017.
 */
public class TestAdvancedAttackingStrategy {
    private FixedDieRollStrategy fixedDieRollStrategy;
    private AttackingStrategy attackingStrategy;
    @Before
    public void setUp() throws Exception {
        fixedDieRollStrategy = new FixedDieRollStrategy(1,1);
        attackingStrategy = new AdvancedAttackingStrategy(fixedDieRollStrategy);


    }

    @Test
    // archer attack strength = 2*2
    // legion defensive strength = 2*1
    // 2*2 > 2*1
    public void RedArcherWithDieRoll2ShouldWinOverBlueLegionWithDieRoll1() {
        FixedDieRollStrategy fixedDieRollStrategy = new FixedDieRollStrategy(2,1);
        AttackingStrategy attackingStrategy = new AdvancedAttackingStrategy(fixedDieRollStrategy);
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        assertThat(attackingStrategy.resolveAttack(new Position(2,0), new Position(3,2), (GameImpl)game), is(true));

    }

    @Test
    // archer attack strength = 2*2
    // legion defensive strength = 2*4
    // 2*2 !> 2*4
    public void RedArcherWithDieRoll2ShouldNotWinOverBlueLegionWithDieRoll4() {
        FixedDieRollStrategy fixedDieRollStrategy = new FixedDieRollStrategy(2,4);
        AttackingStrategy attackingStrategy = new AdvancedAttackingStrategy(fixedDieRollStrategy);
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        assertThat(attackingStrategy.resolveAttack(new Position(2,0), new Position(3,2), (GameImpl)game), is(false));

    }

    @Test
    public void RedLegionShouldFailAttackVersusArcherOnHill() {
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        assertThat(attackingStrategy.resolveAttack(new Position(11,10), new Position(10,10), (GameImpl)game), is(false));

    }

    @Test
    public void RedLegionShouldFailAttackVersusArcherInForest() {
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        assertThat(attackingStrategy.resolveAttack(new Position(14,13), new Position(13,13), (GameImpl)game), is(false));

    }

    @Test
    public void redLegionShouldFailAttackAgainstArcherLocatedInCity() {
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        assertThat(attackingStrategy.resolveAttack(new Position(4,2), new Position(4,1), (GameImpl)game), is(false));
    }

    @Test
    public void redLegionShouldFailAttackAgainstArcherWithUnitSupport() {
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        assertThat(attackingStrategy.resolveAttack(new Position(7,2), new Position(7,1), (GameImpl)game), is(false));
    }

    @Test
    public void shouldRemoveAttackingArcherAfterUnsuccessfulAttack() {
        FixedDieRollStrategy fixedDieRollStrategy = new FixedDieRollStrategy(2,4);
        AttackingStrategy attackingStrategy = new AdvancedAttackingStrategy(fixedDieRollStrategy);
        Game game = new GameImpl(new EpsilonFixedDieRollFactory(fixedDieRollStrategy));

        attackingStrategy.resolveAttack(new Position(2,0), new Position(3,2), (GameImpl)game);
        assertThat(game.getUnitAt(new Position(2,0)), is(nullValue()));
    }
}
