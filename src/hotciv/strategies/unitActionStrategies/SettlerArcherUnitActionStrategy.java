package hotciv.strategies.unitActionStrategies;
import hotciv.framework.*;
import hotciv.standard.CityImpl;
import hotciv.standard.GameImpl;
import hotciv.standard.UnitImpl;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class SettlerArcherUnitActionStrategy implements UnitActionStrategy {

    private UnitImpl currentUnit;

    @Override
    public void performUnitActionAt(Position p, Game game) {

        currentUnit = (UnitImpl)game.getUnitAt(p);

        //Settler Action: make a city
        if(currentUnit.getTypeString().equals(GameConstants.SETTLER)) {
            ((GameImpl)game).createCity(p, new CityImpl(currentUnit.getOwner()));
            ((GameImpl)game).removeUnit(p);

        //Archer Action: Fortify (defence = 6, not movable)
        } else if (currentUnit.getTypeString().equals(GameConstants.ARCHER)){
            int strength = currentUnit.getDefensiveStrength();
            if(strength == 3) {
                currentUnit.setDefensiveStrength(6);
                currentUnit.setMovable(false);
            } else {
                currentUnit.setDefensiveStrength(3);
                currentUnit.setMovable(true);
            }
        }
    }
}
