package hotciv.strategies.unitActionStrategies;
import hotciv.framework.*;


/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class NoUnitActionStrategy implements UnitActionStrategy {
    @Override
    public void performUnitActionAt(Position p, Game game) {

    }
}
