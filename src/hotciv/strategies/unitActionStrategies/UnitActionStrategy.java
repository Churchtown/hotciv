package hotciv.strategies.unitActionStrategies;

import hotciv.framework.Game;
import hotciv.framework.Position;


/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public interface UnitActionStrategy {

    void performUnitActionAt(Position p, Game game);
}
