package hotciv.strategies.winnerStrategies;
import hotciv.framework.*;
import hotciv.standard.GameImpl;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class CityWinnerStrategy implements WinnerStrategy {
    @Override
    public Player getWinner(GameImpl game) {
        //No winner per default
        Player result = null;

        //loops through all cities and checks if they have the same owner - returns null if not.
        for(Position p: game.getCities()) {
            Player owner = game.getCityAt(p).getOwner();

            if(result==null){
                result = owner;
            } else if(result!=owner) {
                return null;
            }
        }
        return result;
    }

    @Override
    public void updateWinCondition(Game game) { }
}
