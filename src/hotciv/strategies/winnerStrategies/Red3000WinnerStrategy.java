package hotciv.strategies.winnerStrategies;
import hotciv.framework.*;
import hotciv.standard.GameImpl;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class Red3000WinnerStrategy implements WinnerStrategy {
    @Override
    public Player getWinner(GameImpl game) {
        if(game.getAge() == -3000) {
            return Player.RED;
        }
        return null;
    }

    @Override
    public void updateWinCondition(Game game) { }
}
