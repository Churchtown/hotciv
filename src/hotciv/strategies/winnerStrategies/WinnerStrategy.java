package hotciv.strategies.winnerStrategies;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.standard.GameImpl;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public interface WinnerStrategy {
    Player getWinner(GameImpl game);

    void updateWinCondition(Game game);
}
