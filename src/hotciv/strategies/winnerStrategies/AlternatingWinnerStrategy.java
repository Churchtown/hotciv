package hotciv.strategies.winnerStrategies;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.standard.GameImpl;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public class AlternatingWinnerStrategy implements WinnerStrategy {

    private WinnerStrategy threeSuccessfulAttacksWinnerStrategy = new ThreeSuccessfulAttacksWinnerStrategy();
    private WinnerStrategy cityWinnerStrategy = new CityWinnerStrategy();

    @Override
    public Player getWinner(GameImpl game) {

        if (game.getRound() > 20) {
            return threeSuccessfulAttacksWinnerStrategy.getWinner(game);
        } else {
            return cityWinnerStrategy.getWinner(game);
        }
    }

    @Override
    public void updateWinCondition(Game game) {
        if(((GameImpl)game).getRound() == 21){
            ((GameImpl) game).setBlueSuccessfulAttacks(0);
            ((GameImpl) game).setRedSuccessfulAttacks(0);
        }
    }
}
