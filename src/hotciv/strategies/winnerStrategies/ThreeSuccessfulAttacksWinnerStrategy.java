package hotciv.strategies.winnerStrategies;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.standard.GameImpl;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public class ThreeSuccessfulAttacksWinnerStrategy implements WinnerStrategy {
    @Override
    public Player getWinner(GameImpl game) {

        if (game.getBlueSuccessfulAttacks() >= 3) {
            return Player.BLUE;
        }

        if (game.getRedSuccessfulAttacks() >= 3) {
            return Player.RED;
        }

        return null;
    }

    @Override
    public void updateWinCondition(Game game) {  }
}
