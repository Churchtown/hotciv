package hotciv.strategies.attackingStrategies;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.standard.GameImpl;

import static hotciv.standard.Utility.*;

/**
 * Created by soren_kirkeby on 02/10/2017.
 */
public class AdvancedAttackingStrategy implements AttackingStrategy {

    private DieRollStrategy dieRollStrategy;

    public AdvancedAttackingStrategy(DieRollStrategy dieRollStrategy) {
        this.dieRollStrategy = dieRollStrategy;
    }

    @Override
    public boolean resolveAttack(Position from, Position to, Game game) {
        Player AttackingPlayer = game.getUnitAt(from).getOwner();
        Player DefensivePlayer = game.getUnitAt(to).getOwner();

        int attackingUnitStrength = game.getUnitAt(from).getAttackingStrength();
        int defensiveUnitStrength = game.getUnitAt(to).getDefensiveStrength();

        int attackingSupport = getFriendlySupport(game, from, AttackingPlayer);
        int defensiveSupport = getFriendlySupport(game, to, DefensivePlayer);

        int attackingTerrainBonus = getTerrainFactor(game, from);
        int defensiveTerrainBonus = getTerrainFactor(game, to);

        int combinedAttackingStrength = (attackingUnitStrength + attackingSupport) * attackingTerrainBonus;
        int combinedDefensiveStrength = (defensiveUnitStrength + defensiveSupport) * defensiveTerrainBonus;

        int attackingDieRoll = dieRollStrategy.rollAttackingDie();
        int defensiveDieRoll = dieRollStrategy.rollDefensiveDie();

        boolean successfulAttack = (combinedAttackingStrength * attackingDieRoll) > (combinedDefensiveStrength * defensiveDieRoll);

        // if attack is unsuccessful then attacker is removed
        if (!successfulAttack) {
            ((GameImpl)game).removeUnit(from);
        }
        return successfulAttack;
    }

}
