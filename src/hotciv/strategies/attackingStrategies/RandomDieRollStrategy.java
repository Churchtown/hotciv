package hotciv.strategies.attackingStrategies;

import java.util.Random;

/**
 * Created by soren_kirkeby on 02/10/2017.
 */
public class RandomDieRollStrategy implements DieRollStrategy{

    Random randomGenerator;

    public RandomDieRollStrategy() {
        randomGenerator = new Random();
    }

    @Override
    public int rollAttackingDie() {
        return randomGenerator.nextInt(6)+1;
    }

    @Override
    public int rollDefensiveDie() {
        return randomGenerator.nextInt(6)+1;
    }
}
