package hotciv.strategies.attackingStrategies;

import hotciv.framework.Game;
import hotciv.framework.Position;

/**
 * Created by soren_kirkeby on 02/10/2017.
 */
public class SimpleAttackingStrategy implements AttackingStrategy {
    @Override
    public boolean resolveAttack(Position from, Position to, Game game) {
        return true;
    }
}
