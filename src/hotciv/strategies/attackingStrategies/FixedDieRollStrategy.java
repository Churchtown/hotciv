package hotciv.strategies.attackingStrategies;

/**
 * Created by soren_kirkeby on 02/10/2017.
 */
public class FixedDieRollStrategy implements DieRollStrategy {
    private int attackingDieEyes;
    private int defensiveDieEyes;

    public FixedDieRollStrategy(int attackingDieEyes, int defensiveDieEyes) {
        this.attackingDieEyes = attackingDieEyes;
        this.defensiveDieEyes = defensiveDieEyes;
    }

    @Override
    public int rollAttackingDie() {
        return attackingDieEyes;
    }

    @Override
    public int rollDefensiveDie() {
        return defensiveDieEyes;
    }
}
