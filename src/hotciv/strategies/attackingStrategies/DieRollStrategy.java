package hotciv.strategies.attackingStrategies;

/**
 * Created by soren_kirkeby on 02/10/2017.
 */
public interface DieRollStrategy {

    int rollAttackingDie();

    int rollDefensiveDie();
}
