package hotciv.strategies.attackingStrategies;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.standard.GameImpl;

/**
 * Created by soren_kirkeby on 02/10/2017.
 */
public interface AttackingStrategy {
    boolean resolveAttack(Position from, Position to, Game game);
}
