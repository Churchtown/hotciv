package hotciv.strategies.unitSpawnStrategies;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;

public class AlternateUnitSpawnStrategy implements UnitSpawnStrategy {
    @Override
    public boolean isAcceptableSpawnLocation(Position position, Game game, String type) {
        LandUnitSpawnStrategy landUnitSpawnStrategy = new LandUnitSpawnStrategy();
        OceanUnitSpawnStrategy oceanUnitSpawnStrategy = new OceanUnitSpawnStrategy();

        if (type.equals(GameConstants.GALLEY)){
            return oceanUnitSpawnStrategy.isAcceptableSpawnLocation(position, game, type);
        } else {
            return landUnitSpawnStrategy.isAcceptableSpawnLocation(position, game, type);
        }
    }
}
