package hotciv.strategies.unitSpawnStrategies;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.standard.GameImpl;


public class LandUnitSpawnStrategy implements UnitSpawnStrategy {

    public boolean isAcceptableSpawnLocation(Position position, Game game, String type){

    boolean notOccupied = !((GameImpl)game).getUnitMap().containsKey(position);
    boolean notMountains = !game.getTileAt(position).getTypeString().equals(GameConstants.MOUNTAINS);
    boolean notOceans = !game.getTileAt(position).getTypeString().equals(GameConstants.OCEANS);

    return notOccupied && notMountains && notOceans;
}
}
