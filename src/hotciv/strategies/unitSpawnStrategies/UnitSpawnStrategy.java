package hotciv.strategies.unitSpawnStrategies;

import hotciv.framework.Game;
import hotciv.framework.Position;

public interface UnitSpawnStrategy {
    boolean isAcceptableSpawnLocation(Position position, Game game, String type);
}
