package hotciv.strategies.mapStrategies;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.standard.CityImpl;
import hotciv.standard.MapImpl;
import hotciv.standard.TileImpl;
import hotciv.standard.UnitImpl;

/**
 * Created by soren_kirkeby on 13/10/2017.
 */
public class OceanMapStrategy implements MapStrategy {
    private MapImpl map;

    public OceanMapStrategy() {
        map = new MapImpl();
        //Tile initialization
        for(int i=0; i<16; i++){
            for(int j=0; j<16; j++){
                map.getGameMap().put(new Position(i,j), new TileImpl(GameConstants.OCEANS));
            }
        }

        map.getGameMap().put(new Position(1,0), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(0,1), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(2,2), new TileImpl(GameConstants.PLAINS));

        map.getGameMap().put(new Position(10,10), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(10,11), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(10,12), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(11,10), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(11,11), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(11,12), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(12,10), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(12,11), new TileImpl(GameConstants.PLAINS));
        map.getGameMap().put(new Position(12,12), new TileImpl(GameConstants.PLAINS));

        //Unit initialization
        map.getUnitMap().put(new Position(4,0),new UnitImpl(GameConstants.GALLEY, Player.RED));
        map.getUnitMap().put(new Position(2,0),new UnitImpl(GameConstants.GALLEY, Player.RED));
        map.getUnitMap().put(new Position(3,0),new UnitImpl(GameConstants.GALLEY, Player.BLUE));


        //City Initialization
        map.getCityMap().put(new Position(2,2),new CityImpl(Player.RED));
        map.getCityMap().put(new Position(11,11),new CityImpl(Player.RED));

    }

    @Override

    public MapImpl makeMap() {
        return map;
    }
}
