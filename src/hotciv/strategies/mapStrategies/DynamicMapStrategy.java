package hotciv.strategies.mapStrategies;
import hotciv.framework.*;
import hotciv.standard.*;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class DynamicMapStrategy implements MapStrategy {
    private MapImpl map;

    @Override
    public MapImpl makeMap() {
        map = new MapImpl();

        createGameMap();
        createCityMap();
        createUnitMap();
        return map;
    }

    private void createUnitMap() {
        map.getUnitMap().put(new Position(3,8), new UnitImpl(GameConstants.ARCHER, Player.RED));
        map.getUnitMap().put(new Position(4,4), new UnitImpl(GameConstants.LEGION, Player.BLUE));
    }

    private void createCityMap() {
        map.getCityMap().put(new Position(8,12),new CityImpl(Player.RED));
        map.getCityMap().put(new Position(4,5),new CityImpl(Player.BLUE));
    }

    private void createGameMap() {
        /** Define the world as the DeltaCiv layout */
        // Basically we use a 'data driven' approach - code the
        // layout in a simple semi-visual representation, and
        // convert it to the actual Game representation.

        String[] layout =
                new String[]{
                        "...ooMooooo.....",
                        "..ohhoooofffoo..",
                        ".oooooMooo...oo.",
                        ".ooMMMoooo..oooo",
                        "...ofooohhoooo..",
                        ".ofoofooooohhoo.",
                        "...ooo..........",
                        ".ooooo.ooohooM..",
                        ".ooooo.oohooof..",
                        "offfoooo.offoooo",
                        "oooooooo...ooooo",
                        ".ooMMMoooo......",
                        "..ooooooffoooo..",
                        "....ooooooooo...",
                        "..ooohhoo.......",
                        ".....ooooooooo..",
                };
        // Conversion...
        String line;
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            line = layout[r];
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                char tileChar = line.charAt(c);
                String type = "error";
                if (tileChar == '.') {
                    type = GameConstants.OCEANS;
                }
                if (tileChar == 'o') {
                    type = GameConstants.PLAINS;
                }
                if (tileChar == 'M') {
                    type = GameConstants.MOUNTAINS;
                }
                if (tileChar == 'f') {
                    type = GameConstants.FOREST;
                }
                if (tileChar == 'h') {
                    type = GameConstants.HILLS;
                }
                Position p = new Position(r, c);
                map.getGameMap().put(p, new TileImpl(type));
            }
        }
    }
}

