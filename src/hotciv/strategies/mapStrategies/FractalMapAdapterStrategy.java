package hotciv.strategies.mapStrategies;

import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.standard.MapImpl;
import hotciv.standard.TileImpl;
import hotciv.standard.Utility;
import thirdparty.ThirdPartyFractalGenerator;

import java.util.HashMap;

/**
 * Created by soren_kirkeby on 01/11/2017.
 */
public class FractalMapAdapterStrategy implements MapStrategy {
    private ThirdPartyFractalGenerator generator = new ThirdPartyFractalGenerator();
    private MapImpl map = new MapImpl();
    private HashMap<Position, TileImpl> gameMap = map.getGameMap();


    @Override
    public MapImpl makeMap() {
        for (int r = 0; r < GameConstants.WORLDSIZE; r++ ) {
            for ( int c = 0; c < GameConstants.WORLDSIZE; c++ ) {


                 char tile = generator.getLandscapeAt(r,c);

                switch (tile) {
                    case '.':
                        gameMap.put(new Position(r, c), new TileImpl(GameConstants.OCEANS));
                        break;
                    case 'o':
                        gameMap.put(new Position(r, c), new TileImpl(GameConstants.PLAINS));
                        break;
                    case 'M':
                        gameMap.put(new Position(r, c), new TileImpl(GameConstants.MOUNTAINS));
                        break;
                    case 'f':
                        gameMap.put(new Position(r, c), new TileImpl(GameConstants.FOREST));
                        break;
                    case 'h':
                        gameMap.put(new Position(r, c), new TileImpl(GameConstants.HILLS));
                        break;
                }
            }
        }
        return map;
    }
}
