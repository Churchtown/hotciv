package hotciv.strategies.mapStrategies;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.standard.CityImpl;
import hotciv.standard.MapImpl;
import hotciv.standard.TileImpl;
import hotciv.standard.UnitImpl;

/**
 * Created by soren_kirkeby on 04/10/2017.
 * Map with lots of units in the beginning to test strategies that require lots of units
 */
public class LotsOfUnitsMapStrategy implements MapStrategy {

    private MapImpl map;

    public LotsOfUnitsMapStrategy() {
        map = new MapImpl();
        //Tile initialization
        for(int i=0; i<16; i++){
            for(int j=0; j<16; j++){
                map.getGameMap().put(new Position(i,j), new TileImpl(GameConstants.PLAINS));
            }
        }
        map.getGameMap().put(new Position(1,0), new TileImpl(GameConstants.OCEANS));
        map.getGameMap().put(new Position(0,1), new TileImpl(GameConstants.HILLS));
        map.getGameMap().put(new Position(2,2), new TileImpl(GameConstants.MOUNTAINS));
        map.getGameMap().put(new Position(10,10), new TileImpl(GameConstants.HILLS));
        map.getGameMap().put(new Position(13,13), new TileImpl(GameConstants.FOREST));


        //Unit initialization
        map.getUnitMap().put(new Position(2,0),new UnitImpl(GameConstants.ARCHER, Player.RED));
        map.getUnitMap().put(new Position(3,2),new UnitImpl(GameConstants.LEGION,Player.BLUE));
        map.getUnitMap().put(new Position(4,3),new UnitImpl(GameConstants.SETTLER,Player.RED));
        map.getUnitMap().put(new Position(2,5),new UnitImpl(GameConstants.ARCHER, Player.RED));
        map.getUnitMap().put(new Position(2,6),new UnitImpl(GameConstants.ARCHER, Player.RED));
        map.getUnitMap().put(new Position(2,7),new UnitImpl(GameConstants.ARCHER, Player.RED));
        map.getUnitMap().put(new Position(4,2),new UnitImpl(GameConstants.ARCHER, Player.RED));
        map.getUnitMap().put(new Position(3,5),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(3,6),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(3,7),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(10,10),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(11,10),new UnitImpl(GameConstants.LEGION, Player.RED));
        map.getUnitMap().put(new Position(13,13),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(14,13),new UnitImpl(GameConstants.LEGION, Player.RED));
        map.getUnitMap().put(new Position(4,1),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(4,2),new UnitImpl(GameConstants.LEGION, Player.RED));
        map.getUnitMap().put(new Position(7,1),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(8,1),new UnitImpl(GameConstants.ARCHER, Player.BLUE));
        map.getUnitMap().put(new Position(7,2),new UnitImpl(GameConstants.LEGION, Player.RED));


        //City Initialization
        map.getCityMap().put(new Position(1,1),new CityImpl(Player.RED));
        map.getCityMap().put(new Position(4,1),new CityImpl(Player.BLUE));
    }


    @Override
    public MapImpl makeMap() {
        return map;
    }
}

