package hotciv.strategies.mapStrategies;

import hotciv.standard.MapImpl;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public interface MapStrategy {
    MapImpl makeMap();
}
