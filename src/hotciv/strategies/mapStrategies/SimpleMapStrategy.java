package hotciv.strategies.mapStrategies;
import hotciv.framework.*;
import hotciv.standard.*;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class SimpleMapStrategy implements MapStrategy {
    private MapImpl map;

    public SimpleMapStrategy() {
        map = new MapImpl();
        //Tile initialization
        for(int i=0; i<16; i++){
            for(int j=0; j<16; j++){
                map.getGameMap().put(new Position(i,j), new TileImpl(GameConstants.PLAINS));
            }
        }
        map.getGameMap().put(new Position(1,0), new TileImpl(GameConstants.OCEANS));
        map.getGameMap().put(new Position(0,1), new TileImpl(GameConstants.HILLS));
        map.getGameMap().put(new Position(2,2), new TileImpl(GameConstants.MOUNTAINS));


        //Unit initialization
        map.getUnitMap().put(new Position(2,0),new UnitImpl(GameConstants.ARCHER, Player.RED));
        map.getUnitMap().put(new Position(3,2),new UnitImpl(GameConstants.LEGION,Player.BLUE));
        map.getUnitMap().put(new Position(4,3),new UnitImpl(GameConstants.SETTLER,Player.RED));

        //City Initialization
        map.getCityMap().put(new Position(1,1),new CityImpl(Player.RED));
        map.getCityMap().put(new Position(4,1),new CityImpl(Player.BLUE));
    }

    @Override

    public MapImpl makeMap() {
        return map;
    }
}
