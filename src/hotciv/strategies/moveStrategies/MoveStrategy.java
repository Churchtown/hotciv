package hotciv.strategies.moveStrategies;

import hotciv.framework.Position;
import hotciv.standard.GameImpl;

/**
 * Created by soren_kirkeby on 13/10/2017.
 */
public interface MoveStrategy {

    boolean isValidToMove(Position from, Position to, GameImpl game);
}
