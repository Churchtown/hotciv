package hotciv.strategies.moveStrategies;

import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.standard.GameImpl;
import hotciv.standard.UnitImpl;

public class AlternateMoveStrategy implements MoveStrategy {
    @Override
    public boolean isValidToMove(Position from, Position to, GameImpl game) {
        OceanMoveStrategy oceanMoveStrategy = new OceanMoveStrategy();
        LandMoveStrategy landMoveStrategy = new LandMoveStrategy();
        UnitImpl currentUnit = (UnitImpl)game.getUnitAt(from);

        if(currentUnit.getTypeString().equals(GameConstants.GALLEY)){
            return oceanMoveStrategy.isValidToMove(from,to,game);
        }else {
            return landMoveStrategy.isValidToMove(from,to,game);
        }
    }
}
