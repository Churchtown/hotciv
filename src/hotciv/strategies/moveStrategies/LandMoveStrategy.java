package hotciv.strategies.moveStrategies;

import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.standard.GameImpl;
import hotciv.standard.MapImpl;
import hotciv.standard.UnitImpl;

/**
 * Created by soren_kirkeby on 13/10/2017.
 */
public class LandMoveStrategy implements MoveStrategy {

    public boolean isValidToMove(Position from, Position to, GameImpl game) {

        if (game.isMountain(to) || game.isOcean(to)) {return false;}
        if (game.notOwnUnit(from) || game.notEnoughMovementPoints(from) || game.notMovable(from) || game.notAdjacentTile(from,to)) {return false;}
        if (game.notAttack(to)) {return false;}

        return true;
    }
}
