package hotciv.strategies.agingStrategies;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public interface AgingStrategy {
    int age(int age);
}
