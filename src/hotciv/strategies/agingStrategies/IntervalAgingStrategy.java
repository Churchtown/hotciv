package hotciv.strategies.agingStrategies;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class IntervalAgingStrategy implements AgingStrategy {
    @Override
    public int age(int age) {
        if(age<-100) {
            return age + 100;
        } else if(age==-100){
            return -1;
        } else if(age==-1){
            return 1;
        } else if(age == 1){
            return 50;
        } else if(age<1750){
            return age + 50;
        } else if(age < 1900){
            return age + 25;
        } else if(age < 1970){
            return age + 5;
        } else { return age + 1; }
    }
}
