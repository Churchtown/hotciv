package hotciv.strategies.agingStrategies;

/**
 * Created by soren_kirkeby on 19/09/2017.
 */
public class LinearAgingStrategy implements AgingStrategy {
    @Override
    public int age(int age) {
        return age + 100;
    }
}
