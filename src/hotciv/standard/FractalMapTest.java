package hotciv.standard;

import hotciv.factories.FractalMapFactory;
import hotciv.framework.Position;

import static org.junit.Assert.assertThat;
import hotciv.framework.*;

/**
 * Created by soren_kirkeby on 01/11/2017.
 */
public class FractalMapTest {

    public static void main(String[] args) {
        Game game = new GameImpl(new FractalMapFactory());

        String line;
        System.out.println("Fractal map made in third party program");

        for ( int r = 0; r < 16; r++ ) {
            line = "";
            String tile = "";
            for ( int c = 0; c < 16; c++ ) {
                switch (game.getTileAt(new Position(r,c)).getTypeString()) {
                    case GameConstants.PLAINS:
                        tile = "o";
                        break;
                    case GameConstants.OCEANS:
                        tile = ".";
                        break;
                    case GameConstants.FOREST:
                        tile = "f";
                        break;
                    case GameConstants.MOUNTAINS:
                        tile = "M";
                        break;
                    case GameConstants.HILLS:
                        tile = "h";
                        break;

                }
                line = line + tile;
            }
            System.out.println(line);
        }
    }
}
