package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Unit;

/**
 * Created by soren_kirkeby on 04/09/2017.
 */
public class UnitImpl implements Unit {

    private String type;
    private Player owner;
    private int moveCount;
    private int attackingStrength;
    private int defensiveStrength;
    private boolean isMovable;

    public UnitImpl(String type, Player owner){
        this.type = type;
        this.owner = owner;
        moveCount = 1;
        isMovable = true;
        switch (type) {
            case GameConstants.ARCHER:
                attackingStrength = 2;
                defensiveStrength = 3;
                break;
            case GameConstants.LEGION:
                attackingStrength = 4;
                defensiveStrength = 2;
                break;
            case GameConstants.SETTLER:
                attackingStrength = 0;
                defensiveStrength = 3;
                break;
            case GameConstants.GALLEY:
                attackingStrength = 8;
                defensiveStrength = 2;
                moveCount = 2;
                break;
        }
    }

    @Override
    public String getTypeString() {
        return type;
    }

    @Override
    public Player getOwner() {
        return owner;
    }

    @Override
    public int getMoveCount() {
        return moveCount;
    }

    @Override
    public int getDefensiveStrength() {
        return defensiveStrength;
    }

    @Override
    public int getAttackingStrength() {
        return attackingStrength;
    }

    public void setMoveCount(int moveCount) {
        this.moveCount = moveCount;
    }

    public void setDefensiveStrength(int defensiveStrength) {
        this.defensiveStrength = defensiveStrength;
    }

    public boolean isMovable() {
        return isMovable;
    }

    public void setMovable(boolean movable) {
        isMovable = movable;
    }
}
