package hotciv.standard;
import hotciv.factories.HotCivFactory;
import hotciv.framework.*;
import hotciv.strategies.moveStrategies.MoveStrategy;
import hotciv.strategies.agingStrategies.AgingStrategy;
import hotciv.strategies.attackingStrategies.AttackingStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.unitActionStrategies.UnitActionStrategy;
import hotciv.strategies.unitSpawnStrategies.UnitSpawnStrategy;
import hotciv.strategies.winnerStrategies.WinnerStrategy;

import java.util.*;

/** Skeleton implementation of HotCiv.

 This source code is from the book
 "Flexible, Reliable Software:
 Using Patterns and Agile Development"
 published 2010 by CRC Press.
 Author:
 Henrik B Christensen
 Department of Computer Science
 Aarhus University

 Please visit http://www.baerbak.com/ for further information.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 */

public class GameImpl implements Game {
    private MapImpl map;
    private Player currentPlayer;
    private int turn;
    private int age;
    private Player winner;

    private AgingStrategy agingStrategy;
    private WinnerStrategy winnerStrategy;
    private UnitActionStrategy unitActionStrategy;
    private UnitSpawnStrategy unitSpawnStrategy;
    private MapStrategy mapStrategy;
    private AttackingStrategy attackingStrategy;
    private MoveStrategy moveStrategy;
    private int redSuccessfulAttacks;
    private int blueSuccessfulAttacks;
    private int round;
    private HashMap<Position, UnitImpl> unitMap;
    private Set<GameObserver> gameObservers;


    public GameImpl(HotCivFactory factory){
        this.agingStrategy = factory.createAgingStrategy();
        this.winnerStrategy = factory.createWinnerStrategy();
        this.unitActionStrategy = factory.createUnitActionStrategy();
        this.unitSpawnStrategy = factory.createUnitSpawnStrategy();
        this.mapStrategy = factory.createMapStrategy();
        this.attackingStrategy = factory.createAttackingStrategy();
        this.moveStrategy = factory.createMoveStrategy();
        redSuccessfulAttacks = 0;
        blueSuccessfulAttacks = 0;

        gameObservers = new HashSet<GameObserver>();

        winner = null;
        map = mapStrategy.makeMap();
        unitMap = map.getUnitMap();

        currentPlayer = Player.RED;
        turn = 1;
        age = -4000;
        round = 0;
    }

    public Tile getTileAt(Position p ) {
        return map.getGameMap().get(p);
    }
    public Unit getUnitAt( Position p ) {
        return map.getUnitAt(p);
    }
    public void removeUnit(Position p) {
        map.getUnitMap().remove(p);
        notifyWorldChange(p);
    }
    public void createCity(Position p, CityImpl city){
        map.getCityMap().put(p,city);
        notifyWorldChange(p);
    }
    public City getCityAt( Position p ) {
        return map.getCityAt(p);
    }
    public Player getPlayerInTurn() { return currentPlayer; }
    public Player getWinner() { return winnerStrategy.getWinner(this); }
    public int getAge() { return age; }
    public boolean moveUnit( Position from, Position to ) {

        if(!isValidToMove(from,to)) {return false;}

        boolean successfulAttack = true;
        boolean isAttack = map.getUnitMap().containsKey(to) && getUnitAt(to).getOwner() != currentPlayer;

        if (isAttack) {
            successfulAttack = attackingStrategy.resolveAttack(from, to, this);
        }

        if (successfulAttack) {
            incrementSuccessfulAttacks(getUnitAt(from).getOwner());
            changeCityOwnerIfCity(to);
            moveUnitToNewPosition(from,to);
        }

        setTileFocus(to);
        notifyWorldChange(to);
        notifyWorldChange(from);
        return true;
    }


    private void incrementSuccessfulAttacks(Player player) {
        if (player == Player.RED) {
            redSuccessfulAttacks += 1;
        } else {
            blueSuccessfulAttacks += 1;
        }
    }

    private void changeCityOwnerIfCity(Position position) {

        boolean isThereACity = map.getCityMap().containsKey(position);
        if(isThereACity) {
            createCity(position, new CityImpl(currentPlayer));
        }
    }


    private void moveUnitToNewPosition(Position from, Position to){
        UnitImpl unit = map.getUnitAt(from);
        map.getUnitMap().remove(from);
        map.getUnitMap().put(to, unit);
        unit.setMoveCount(unit.getMoveCount() - 1);
    }

    //checks if position "to" is a valid location to move, from position "from"
    private boolean isValidToMove(Position from, Position to){

        return moveStrategy.isValidToMove(from, to, this);

    }
    public void endOfTurn() {
        turn += 1;

        //Every second turn age is updated and endOfRound is called.
        boolean everySecondTurn = turn%2==1;
        if(everySecondTurn){
            updateAge();
            endOfRound();
        }

        //After every turn we switch players
        switchPlayer();
        notifyEndOfTurn();
    }


    private void switchPlayer() {
        if(currentPlayer == Player.BLUE){
            currentPlayer = Player.RED;
        } else {
            currentPlayer = Player.BLUE;
        }
    }

    private void updateAge() {
        age = agingStrategy.age(age);
    }

    public void changeWorkForceFocusInCityAt( Position p, String balance ) {}
    public void changeProductionInCityAt( Position p, String unitType ) {
        map.getCityAt(p).setProduction(unitType);
        setTileFocus(p);
    }
    public void performUnitActionAt( Position p ) {
        unitActionStrategy.performUnitActionAt(p, this);
    }



    private void endOfRound() {
        incrementRound();
        updateCities();
        updateUnits();
        updateWinCondition();
    }

    private void updateWinCondition() {
        winnerStrategy.updateWinCondition(this);
    }


    private void updateCities(){
            for(Position p: getCities()) {
                //units are produced
                produceUnit(p);
            }
    }

    private void updateUnits(){
        for(Position p: getUnits()) {
            //units receive movement points
            getMovePoints(p);
        }
    }

    private Set<Position> getUnits(){
        return map.getUnitMap().keySet();
    }

    private void incrementRound() {
        round += 1;
    }

    public Set<Position> getCities(){
        return map.getCityMap().keySet();
    }

    private void produceUnit(Position currentPosition) {

        //cities get production
        cityGetsProduction(currentPosition);

        //sets spawn location
        Position unitSpawn = setSpawnLocation(currentPosition);

        //Produces a unit according to the city's production, on the unitSpawn tile
        produceUnitOnTile(currentPosition,unitSpawn);
    }

    private void produceUnitOnTile(Position currentPosition, Position unitSpawn) {

        if(enoughProduction(currentPosition)){
            try {
                createUnit(currentPosition, unitSpawn);
            } catch(NullPointerException E){
                System.err.println("No Valid Spawn Locations");
            }
        updateProductionNumber(currentPosition);
        }
    }

    private boolean enoughProduction(Position currentPosition){
        CityImpl city = map.getCityAt(currentPosition);
        int currentProduction = city.getProductionNumber();
        String type = getCityProduction(currentPosition);
        switch (type) {
            case GameConstants.SETTLER:
                return currentProduction >= 30;
            case GameConstants.LEGION:
                return currentProduction >= 15;
            case GameConstants.ARCHER:
                return currentProduction >= 10;
            case GameConstants.GALLEY:
                return currentProduction >= 30;
        }
        return false;
    }

    private void updateProductionNumber(Position currentPosition) {
        CityImpl city = map.getCityAt(currentPosition);
        int currentProduction = city.getProductionNumber();
        int cost = 0;
        String type = getCityProduction(currentPosition);

        if (type.equals(GameConstants.ARCHER)){cost = 10;}
        if (type.equals(GameConstants.LEGION)){cost = 15;}
        if (type.equals(GameConstants.SETTLER)){cost = 30;}
        if (type.equals(GameConstants.GALLEY)){cost = 30;}

        city.setProductionNumber(currentProduction - cost);
    }
    //return current unitProduction.
    //if no production set, Archer is returned.
    private String getCityProduction(Position position) {
        String currentUnit = map.getCityAt(position).getProduction();

        if (currentUnit == null) {
            return GameConstants.ARCHER;
        }
        return currentUnit;
    }

    //creates unit appropriate to type, and removes cost from city productionNumber
    private void createUnit(Position cityPosition, Position unitSpawn) {

        String type = getCityProduction(cityPosition);
        Player owner = map.getCityAt(cityPosition).getOwner();

        UnitImpl newUnit = new UnitImpl(type,owner);
        map.getUnitMap().put(unitSpawn, newUnit);

        notifyWorldChange(unitSpawn);
    }

    private void cityGetsProduction(Position position) {
        CityImpl city = map.getCityAt(position);
        int currentProduction = city.getProductionNumber();
        city.setProductionNumber( currentProduction + 6 );
    }

    //units get 1 movement
    private void getMovePoints(Position position) {
            UnitImpl currentUnit = map.getUnitAt(position);
            currentUnit.setMoveCount(1);
    }

    //unitspawn should be the first unoccupied, nonmountain, nonocean field clockwise around the city
    private Position setSpawnLocation(Position position) {

        int j = position.getColumn();
        int i = position.getRow();
        ArrayList<Position> possibleLocations = new ArrayList<>();

        possibleLocations.add(position);
        possibleLocations.add(new Position(i - 1, j));
        possibleLocations.add(new Position(i - 1, j + 1));
        possibleLocations.add(new Position(i, j + 1));
        possibleLocations.add(new Position(i + 1, j + 1));
        possibleLocations.add(new Position(i + 1, j));
        possibleLocations.add(new Position(i + 1, j - 1));
        possibleLocations.add(new Position(i, j - 1));
        possibleLocations.add(new Position(i - 1, j - 1));

            for (Position p : possibleLocations) {
                if (isAcceptableSpawnLocation(p,position)) {
                    return p;
                }
            }
            return null;
    }

    //returning true if the tile at position p is occupied, or if it is a mountain or ocean tile.
    private boolean isAcceptableSpawnLocation(Position position, Position cityPosition){
        return unitSpawnStrategy.isAcceptableSpawnLocation(position, this, getCityProduction(cityPosition));
    }

    public int getRedSuccessfulAttacks() {
        return redSuccessfulAttacks;
    }

    public int getBlueSuccessfulAttacks() {
        return blueSuccessfulAttacks;
    }

    public void setRedSuccessfulAttacks(int i){redSuccessfulAttacks = i;}

    public void setBlueSuccessfulAttacks(int i){blueSuccessfulAttacks = i;}

    public int getRound() {
        return round;
    }

    public HashMap<Position, UnitImpl> getUnitMap() {
        return unitMap;
    }

    public boolean isMountain(Position position) {
        return getTileAt(position).getTypeString().equals(GameConstants.MOUNTAINS);
    }

    public boolean isOcean(Position position) {
        return getTileAt(position).getTypeString().equals(GameConstants.OCEANS);
    }

    public boolean notOwnUnit(Position position) {
        return getUnitAt(position).getOwner() != getPlayerInTurn();
    }

    public boolean notEnoughMovementPoints(Position position) {
        return getUnitAt(position).getMoveCount() < 1;
    }

    public boolean notMovable(Position position) {
        UnitImpl unit = (UnitImpl)getUnitAt(position);
        return !unit.isMovable();
    }

    public boolean notAdjacentTile(Position from, Position to) {
        return (Math.abs(from.getRow() - to.getRow()) > 1) || (Math.abs(from.getColumn() - to.getColumn()) > 1);
    }

    public boolean notAttack(Position position) {

        boolean attack = false;
        boolean enemyUnitOnTile = getUnitMap().containsKey(position) && getUnitAt(position).getOwner() != getPlayerInTurn();
        if (enemyUnitOnTile) {
            attack = true;
        }

        return !(!getUnitMap().containsKey(position) || attack);
    }

    @Override
    public void addObserver(GameObserver observer) {
        gameObservers.add(observer);
    }

    @Override
    public void setTileFocus(Position position) {
        for (GameObserver go: gameObservers) {
            go.tileFocusChangedAt(position);
        }
    }

    private void notifyWorldChange(Position from) {
        for (GameObserver gameObserver: gameObservers) {
            gameObserver.worldChangedAt(from);
        }
    }

    private void notifyEndOfTurn() {
        for (GameObserver gameObserver: gameObservers) {
            gameObserver.turnEnds(getPlayerInTurn(), getAge());
        }
    }
}

