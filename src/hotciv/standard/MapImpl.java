package hotciv.standard;
import hotciv.framework.*;

import java.util.HashMap;

/**
 * Created by tiami on 04/09/2017.
 */
public class MapImpl {
    //Map
    private HashMap<Position,TileImpl> gameMap;
    private HashMap<Position,UnitImpl> unitMap;
    private HashMap<Position,CityImpl> cityMap;

    public MapImpl() {
        gameMap = new HashMap<>();
        unitMap = new HashMap<>();
        cityMap = new HashMap<>();
    }

    public String getTileType(Position position) {

        return gameMap.get(position).getTypeString();
    }

    public UnitImpl getUnitAt (Position position) {return unitMap.get(position);}
    public String getUnitType (Position position) {return unitMap.get(position).getTypeString();}
    public CityImpl getCityAt (Position position) {
        return cityMap.get(position);
    }
    public HashMap<Position,TileImpl> getGameMap() {
        return gameMap;
    }

    public HashMap<Position,UnitImpl> getUnitMap() {
        return unitMap;
    }

    public HashMap<Position, CityImpl> getCityMap() {
        return cityMap;
    }
}
