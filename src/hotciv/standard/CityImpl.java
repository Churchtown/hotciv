package hotciv.standard;
import hotciv.framework.*;

/**
 * Created by tiami on 04/09/2017.
 */
public class CityImpl implements City {

    private Player owner;
    private String production;
    private int productionNumber;

    public CityImpl(Player owner) {
        this.owner = owner;
        productionNumber = 0;
        production = GameConstants.ARCHER;
    }

    @Override
    public Player getOwner() {
        return owner;
    }

    @Override
    public int getSize() {
        return 1;
    }

    @Override
    public String getProduction() {
        return production;
    }

    public  void setProduction(String production){
        this.production = production;
    }

    @Override
    public String getWorkforceFocus() {
        return null;
    }

    public int getProductionNumber() {
        return productionNumber;
    }

    public void setProductionNumber(int productionNumber) {
        this.productionNumber = productionNumber;
    }
}
