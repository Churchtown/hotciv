package hotciv.standard;

import hotciv.framework.*;

public class Transcriber implements Game{
    private Game game;

    public Transcriber(Game game) {
        this.game = game;
    }

    @Override
    public Tile getTileAt(Position p) {
        return game.getTileAt(p);
    }

    @Override
    public Unit getUnitAt(Position p) {
        return game.getUnitAt(p);
    }

    @Override
    public City getCityAt(Position p) {
        return game.getCityAt(p);
    }

    @Override
    public Player getPlayerInTurn() {
        return game.getPlayerInTurn();
    }

    @Override
    public Player getWinner() {
        return game.getWinner();
    }

    @Override
    public int getAge() {
        return game.getAge();
    }

    @Override
    public boolean moveUnit(Position from, Position to) {

        boolean isMoved = game.moveUnit(from,to);

        if(isMoved) {
            String player = game.getPlayerInTurn().toString();
            String unit = game.getUnitAt(to).getTypeString();

            StringBuilder sb = new StringBuilder();
            sb.append(player);
            sb.append(" moves ");
            sb.append(unit);
            sb.append(" from ");
            sb.append(from.toString());
            sb.append(" to ");
            sb.append(to.toString());
            sb.append(".");

            System.out.println(sb.toString());

        }

        return isMoved;
    }

    @Override
    public void endOfTurn() {
        String currentPlayer = game.getPlayerInTurn().toString();

        System.out.println(currentPlayer + " ends turn." );
        game.endOfTurn();

    }

    @Override
    public void changeWorkForceFocusInCityAt(Position p, String balance) {
        game.changeWorkForceFocusInCityAt(p,balance);
    }

    @Override
    public void changeProductionInCityAt(Position p, String unitType) {

        String player = game.getPlayerInTurn().toString();

        System.out.println(player + " changes production in city at " + p.toString() + " to " + unitType + ".");
        game.changeProductionInCityAt(p,unitType);
    }

    @Override
    public void performUnitActionAt(Position p) {
        Unit unit = game.getUnitAt(p);

        System.out.println(unit.getOwner().toString() + " " + unit.getTypeString() + " performs its unit action.");

        game.performUnitActionAt(p);
    }

    @Override
    public void addObserver(GameObserver observer) {

    }

    @Override
    public void setTileFocus(Position position) {

    }

    public Game getGame() {
        return game;
    }

}
