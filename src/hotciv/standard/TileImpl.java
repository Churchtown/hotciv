package hotciv.standard;

import hotciv.framework.Tile;

/**
 * Created by soren_kirkeby on 30/08/2017.
 */
public class TileImpl implements Tile {
    private String type;

    public TileImpl(String type) {
        this.type = type;
    }

    @Override
    public String getTypeString() {
        return type;
    }

}
