package hotciv.factories;

import hotciv.strategies.mapStrategies.FractalMapAdapterStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;

/**
 * Created by soren_kirkeby on 01/11/2017.
 */
public class FractalMapFactory extends AlphaCivFactory {
    @Override
    public MapStrategy createMapStrategy() {
        return new FractalMapAdapterStrategy();
    }
}
