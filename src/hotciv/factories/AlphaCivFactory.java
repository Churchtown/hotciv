package hotciv.factories;

import hotciv.strategies.agingStrategies.AgingStrategy;
import hotciv.strategies.agingStrategies.LinearAgingStrategy;
import hotciv.strategies.attackingStrategies.AttackingStrategy;
import hotciv.strategies.attackingStrategies.SimpleAttackingStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.mapStrategies.SimpleMapStrategy;
import hotciv.strategies.moveStrategies.LandMoveStrategy;
import hotciv.strategies.moveStrategies.MoveStrategy;
import hotciv.strategies.unitActionStrategies.NoUnitActionStrategy;
import hotciv.strategies.unitActionStrategies.UnitActionStrategy;
import hotciv.strategies.unitSpawnStrategies.LandUnitSpawnStrategy;
import hotciv.strategies.unitSpawnStrategies.UnitSpawnStrategy;
import hotciv.strategies.winnerStrategies.Red3000WinnerStrategy;
import hotciv.strategies.winnerStrategies.WinnerStrategy;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public class AlphaCivFactory implements HotCivFactory {
    @Override
    public WinnerStrategy createWinnerStrategy() {
        return new Red3000WinnerStrategy();
    }

    @Override
    public MapStrategy createMapStrategy() {
        return new SimpleMapStrategy();
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new NoUnitActionStrategy();
    }

    @Override
    public AgingStrategy createAgingStrategy() {
        return new LinearAgingStrategy();
    }

    @Override
    public AttackingStrategy createAttackingStrategy() {
        return new SimpleAttackingStrategy();
    }

    @Override
    public MoveStrategy createMoveStrategy() {
        return new LandMoveStrategy();
    }

    @Override
    public UnitSpawnStrategy createUnitSpawnStrategy() {
        return new LandUnitSpawnStrategy();
    }
}
