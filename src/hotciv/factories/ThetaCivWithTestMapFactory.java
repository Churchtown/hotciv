package hotciv.factories;

import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.mapStrategies.OceanMapStrategy;

/**
 * Created by soren_kirkeby on 13/10/2017.
 */
public class ThetaCivWithTestMapFactory extends ThetaCivFactory {
    @Override
    public MapStrategy createMapStrategy() {
        return new OceanMapStrategy();
    }
}
