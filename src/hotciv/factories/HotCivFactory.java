package hotciv.factories;

import hotciv.strategies.agingStrategies.AgingStrategy;
import hotciv.strategies.attackingStrategies.AttackingStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.moveStrategies.MoveStrategy;
import hotciv.strategies.unitActionStrategies.UnitActionStrategy;
import hotciv.strategies.unitSpawnStrategies.UnitSpawnStrategy;
import hotciv.strategies.winnerStrategies.WinnerStrategy;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public interface HotCivFactory {
    WinnerStrategy createWinnerStrategy();

    MapStrategy createMapStrategy();

    UnitActionStrategy createUnitActionStrategy();

    AgingStrategy createAgingStrategy();

    AttackingStrategy createAttackingStrategy();

    MoveStrategy createMoveStrategy();

    UnitSpawnStrategy createUnitSpawnStrategy();
}
