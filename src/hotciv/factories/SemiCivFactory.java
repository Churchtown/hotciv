package hotciv.factories;

import hotciv.strategies.agingStrategies.AgingStrategy;
import hotciv.strategies.agingStrategies.IntervalAgingStrategy;
import hotciv.strategies.attackingStrategies.AdvancedAttackingStrategy;
import hotciv.strategies.attackingStrategies.AttackingStrategy;
import hotciv.strategies.attackingStrategies.RandomDieRollStrategy;
import hotciv.strategies.mapStrategies.DynamicMapStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.moveStrategies.LandMoveStrategy;
import hotciv.strategies.moveStrategies.MoveStrategy;
import hotciv.strategies.unitActionStrategies.SettlerArcherUnitActionStrategy;
import hotciv.strategies.unitActionStrategies.UnitActionStrategy;
import hotciv.strategies.unitSpawnStrategies.LandUnitSpawnStrategy;
import hotciv.strategies.unitSpawnStrategies.UnitSpawnStrategy;
import hotciv.strategies.winnerStrategies.AlternatingWinnerStrategy;
import hotciv.strategies.winnerStrategies.WinnerStrategy;

/**
 * Created by soren_kirkeby on 11/10/2017.
 */
public class SemiCivFactory implements HotCivFactory {
    @Override
    public WinnerStrategy createWinnerStrategy() {
        return new AlternatingWinnerStrategy();
    }

    @Override
    public MapStrategy createMapStrategy() {
        return new DynamicMapStrategy();
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new SettlerArcherUnitActionStrategy();
    }

    @Override
    public AgingStrategy createAgingStrategy() {
        return new IntervalAgingStrategy();
    }

    @Override
    public AttackingStrategy createAttackingStrategy() {
        return new AdvancedAttackingStrategy(new RandomDieRollStrategy());
    }

    @Override
    public MoveStrategy createMoveStrategy() {
        return new LandMoveStrategy();
    }

    @Override
    public UnitSpawnStrategy createUnitSpawnStrategy() {
        return new LandUnitSpawnStrategy();
    }
}
