package hotciv.factories;

import hotciv.strategies.agingStrategies.AgingStrategy;
import hotciv.strategies.agingStrategies.LinearAgingStrategy;
import hotciv.strategies.attackingStrategies.AdvancedAttackingStrategy;
import hotciv.strategies.attackingStrategies.AttackingStrategy;
import hotciv.strategies.attackingStrategies.DieRollStrategy;
import hotciv.strategies.mapStrategies.LotsOfUnitsMapStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.moveStrategies.LandMoveStrategy;
import hotciv.strategies.moveStrategies.MoveStrategy;
import hotciv.strategies.unitActionStrategies.NoUnitActionStrategy;
import hotciv.strategies.unitActionStrategies.UnitActionStrategy;
import hotciv.strategies.winnerStrategies.ThreeSuccessfulAttacksWinnerStrategy;
import hotciv.strategies.winnerStrategies.WinnerStrategy;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public class EpsilonFixedDieRollFactory extends EpsilonCivFactory implements HotCivFactory {

    DieRollStrategy fixedDie;

    public EpsilonFixedDieRollFactory(DieRollStrategy fixedDie) {
        this.fixedDie = fixedDie;
    }

    @Override
    public MapStrategy createMapStrategy() {
        return new LotsOfUnitsMapStrategy();
    }

    @Override
    public AttackingStrategy createAttackingStrategy() {
        return new AdvancedAttackingStrategy(fixedDie);
    }

    @Override
    public MoveStrategy createMoveStrategy() {
        return new LandMoveStrategy();
    }
}
