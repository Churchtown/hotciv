package hotciv.factories;

import hotciv.strategies.mapStrategies.LotsOfUnitsMapStrategy;
import hotciv.strategies.mapStrategies.MapStrategy;
import hotciv.strategies.moveStrategies.LandMoveStrategy;
import hotciv.strategies.moveStrategies.MoveStrategy;

/**
 * Created by soren_kirkeby on 04/10/2017.
 */
public class ZetaCivWithTestMapFactory extends ZetaCivFactory implements HotCivFactory {

    @Override
    public MapStrategy createMapStrategy() {
        return new LotsOfUnitsMapStrategy();
    }

    @Override
    public MoveStrategy createMoveStrategy() {
        return new LandMoveStrategy();
    }

}
