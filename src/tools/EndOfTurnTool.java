package tools;

import hotciv.framework.Game;
import hotciv.view.EndTurnFigure;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;
import minidraw.framework.Tool;
import minidraw.standard.ImageFigure;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created by soren_kirkeby on 08/11/2017.
 */
public class EndOfTurnTool implements Tool {

    private final DrawingEditor editor;
    private final Game game;
    private boolean notDragged = true;

    public EndOfTurnTool(DrawingEditor editor, Game game) {
        this.editor = editor;
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) { notDragged = false; }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        if(notDragged) {
            Figure figure = editor.drawing().findFigure(i, i1);
            if (figure != null && figure instanceof EndTurnFigure) {
                game.endOfTurn();
            }
        } else {
            notDragged = true;
        }
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {    }
}
