package tools;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import hotciv.view.UnitFigure;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Created by soren_kirkeby on 07/11/2017.
 */
public class UnitMoveTool implements minidraw.framework.Tool {

    private Figure selectedUnit;
    private int x;
    private int y;
    private int originalX;
    private int originalY;
    private Position originalPos;

    private DrawingEditor editor;
    private Game game;


    public UnitMoveTool(DrawingEditor editor, Game game) {
        this.editor = editor;
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        Figure figure = editor.drawing().findFigure(i, i1);
        if (figure != null && figure instanceof UnitFigure) {
            selectedUnit = figure;
        }

        x = i;
        y = i1;
        originalX = i;
        originalY = i1;
        originalPos = GfxConstants.getPositionFromXY(i,i1);
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {
        if (isThereAUnit()) {
            selectedUnit.moveBy(i - x, i1 - y);
            x = mouseEvent.getX();
            y = mouseEvent.getY();
        }
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        Position newPosition = GfxConstants.getPositionFromXY(x, y);
        int xFromColumn = GfxConstants.getXFromColumn(newPosition.getColumn());
        int yFromRow = GfxConstants.getYFromRow(newPosition.getRow());
        int xPos = xFromColumn - x;
        int yPos = yFromRow - y;

        if (isThereAUnit()){
            if(!originalPos.equals(newPosition)) {
                if (game.moveUnit(originalPos, newPosition)) {
                    selectedUnit.moveBy(xPos, yPos);
                } else {
                    selectedUnit.moveBy(originalX - x,
                            originalY - y);
                }
            }
        }
        selectedUnit = null;
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {    }

    private boolean isThereAUnit(){
        return selectedUnit != null;
    }
}
