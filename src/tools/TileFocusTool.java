package tools;

import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class TileFocusTool implements Tool{

    private boolean notDragged = true;
    private Game game;

    public TileFocusTool(Game game) {
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {
        notDragged = false;
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {

        Position position = GfxConstants.getPositionFromXY(i, i1);
        int yPos = position.getColumn();
        int xPos = position.getRow();
        if(notDragged && xPos < GameConstants.WORLDSIZE && yPos < GameConstants.WORLDSIZE){
            Position pos = new Position(xPos,yPos);
            System.out.println("TILE FOCUS changed to: " + pos.toString());
            game.setTileFocus(pos);
        } else {
            notDragged = true;
        }
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {    }
}
