package tools;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class ActionTool implements Tool {

    private Game game;
    public ActionTool(Game game) {
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        Position position = GfxConstants.getPositionFromXY(i, i1);
        int yPos = position.getColumn();
        int xPos = position.getRow();
        boolean isThereAUnit = game.getUnitAt(new Position(xPos,yPos))!=null;
        if(mouseEvent.isShiftDown() && isThereAUnit){
        game.performUnitActionAt(new Position(xPos,yPos));
        }
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {    }
}
