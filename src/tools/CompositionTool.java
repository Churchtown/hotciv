package tools;

import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class CompositionTool implements Tool {

    private ArrayList<Tool> tools;

    public CompositionTool(){
        tools = new ArrayList<>();
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int i, int i1) {
        for(Tool tool: tools){
            tool.mouseDown(mouseEvent,i,i1);
        }
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int i, int i1) {
        for(Tool tool: tools){
            tool.mouseDrag(mouseEvent,i,i1);
        }
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int i, int i1) {
        for(Tool tool: tools){
            tool.mouseUp(mouseEvent,i,i1);
        }
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int i, int i1) {
        for(Tool tool: tools){
            tool.mouseMove(mouseEvent,i,i1);
        }
    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {
        for(Tool tool: tools){
            tool.keyDown(keyEvent,i);
        }
    }

    public void addTool(Tool tool){
        tools.add(tool);
    }
}
